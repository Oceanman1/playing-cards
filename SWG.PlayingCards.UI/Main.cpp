#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit
{
	CLUBS,
	SPADES,
	HEARTS,
	DIAMONDS
};


struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	string rank;
	string suit;
	if (card.rank == 0) {
		rank = "Two";
	}
	else if (card.rank == 1) {
		rank = "Three";
	}
	else if (card.rank == 2) {
		rank = "Four";
	}
	else if (card.rank == 3) {
		rank = "Five";
	}
	else if (card.rank == 4) {
		rank = "Six";
	}
	else if (card.rank == 5) {
		rank = "Seven";
	}
	else if (card.rank == 6) {
		rank = "Eight";
	}
	else if (card.rank == 7) {
		rank = "Nine";
	}
	else if (card.rank == 8) {
		rank = "Ten";
	}
	else if (card.rank == 9) {
		rank = "Jack";
	}
	else if (card.rank == 10) {
		rank = "Queen";
	}
	else if (card.rank == 11) {
		rank = "King";
	}
	else if (card.rank == 12) {
		rank = "Ace";
	}

	if (card.suit == 0) {
		suit = "Clubs";
	}
	else if (card.suit == 1) {
		suit = "Spades";
	}
	else if (card.suit == 2) {
		suit = "Hearts";
	}
	else if (card.suit == 3) {
		suit = "Diamonds";
	}

	cout << "The " << rank << " of " << suit << "\n";
}

Card HighCard(Card card1, Card card2)
{
	string rank1;
	string rank2;
	string suit1;
	string suit2;

	if (card1.rank == 0) {
		rank1 = "Two";
	}
	else if (card1.rank == 1) {
		rank1 = "Three";
	}
	else if (card1.rank == 2) {
		rank1 = "Four";
	}
	else if (card1.rank == 3) {
		rank1 = "Five";
	}
	else if (card1.rank == 4) {
		rank1 = "Six";
	}
	else if (card1.rank == 5) {
		rank1 = "Seven";
	}
	else if (card1.rank == 6) {
		rank1 = "Eight";
	}
	else if (card1.rank == 7) {
		rank1 = "Nine";
	}
	else if (card1.rank == 8) {
		rank1 = "Ten";
	}
	else if (card1.rank == 9) {
		rank1 = "Jack";
	}
	else if (card1.rank == 10) {
		rank1 = "Queen";
	}
	else if (card1.rank == 11) {
		rank1 = "King";
	}
	else if (card1.rank == 12) {
		rank1 = "Ace";
	}

	if (card2.rank == 0) {
		rank2 = "Two";
	}
	else if (card2.rank == 1) {
		rank2 = "Three";
	}
	else if (card2.rank == 2) {
		rank2 = "Four";
	}
	else if (card2.rank == 3) {
		rank2 = "Five";
	}
	else if (card2.rank == 4) {
		rank2 = "Six";
	}
	else if (card2.rank == 5) {
		rank2 = "Seven";
	}
	else if (card2.rank == 6) {
		rank2 = "Eight";
	}
	else if (card2.rank == 7) {
		rank2 = "Nine";
	}
	else if (card2.rank == 8) {
		rank2 = "Ten";
	}
	else if (card2.rank == 9) {
		rank2 = "Jack";
	}
	else if (card2.rank == 10) {
		rank2 = "Queen";
	}
	else if (card2.rank == 11) {
		rank2 = "King";
	}
	else if (card2.rank == 12) {
		rank2 = "Ace";
	}

	if (card1.suit == 0) {
		suit1 = "Clubs";
	}
	else if (card1.suit == 1) {
		suit1 = "Spades";
	}
	else if (card1.suit == 2) {
		suit1 = "Hearts";
	}
	else if (card1.suit == 3) {
		suit1 = "Diamonds";
	}

	if (card2.suit == 0) {
		suit2 = "Clubs";
	}
	else if (card2.suit == 1) {
		suit2 = "Spades";
	}
	else if (card2.suit == 2) {
		suit2 = "Hearts";
	}
	else if (card2.suit == 3) {
		suit2 = "Diamonds";
	}
	if (card1.rank > card2.rank) {
		cout << rank1 << " of " << suit1 << " is the winner!";
		return card1;
	}
	else if (card1.rank < card2.rank) {
		cout << rank2 << " of " << suit2 << " is the winner!";
		return card2;
	}
}

int main()
{

	Card QueenOfHearts;

	QueenOfHearts.suit = HEARTS;
	QueenOfHearts.rank = QUEEN;

	Card JackOfHearts;

	JackOfHearts.suit = HEARTS;
	JackOfHearts.rank = JACK;

	PrintCard(QueenOfHearts);
	HighCard(QueenOfHearts, JackOfHearts);



	_getch();
	return 0;
}